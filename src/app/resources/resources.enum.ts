export enum Industries {
    'Agriculture',
    'Chemical',
    'Foodservice',
    'Manufaturing',
    'Mill Products',
    'Retail',
    'Wholesale Distribution',
    'Automotive',
    'Consumer Products',
    'High Tech',
    'Life Science',
    'Music, Media & Licensing',
    'Telecommunications'
}

export enum Services {
    'Analytics',
    'CRM Integration',
    'Program Delivery',
    'Solution Delivery',
    'Customer Support',
    'Global Payments',
    'Strategy',
    'Training'
}

export enum Solutions {
    'Go-to-Market Suite',
    'Solutions for SAP',
    'channelConduit Suite',
    'Counterpoint Suite',
    'VIBES'
}
