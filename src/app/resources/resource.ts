export interface Resource {

    image: string;
    blurb: string;
    link: string;

    isVisible: boolean;

    industry?: string;
    service?: string;
    solution?: string;

}
